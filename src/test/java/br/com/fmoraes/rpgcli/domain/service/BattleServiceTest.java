package br.com.fmoraes.rpgcli.domain.service;

import br.com.fmoraes.rpgcli.domain.model.character.Character;
import br.com.fmoraes.rpgcli.domain.model.monsters.Monster;
import br.com.fmoraes.rpgcli.domain.model.monsters.SmallMonster;
import org.junit.Test;

import static org.junit.Assert.*;

public class BattleServiceTest {

    private BattleService battleService = new BattleService();

    @Test
    public void testCalculateAttackDamage() {
        Character character = new Character.Builder("Test").build();
        Monster smallMonster = new SmallMonster();

        battleService.calculateAttackDamage(character, smallMonster);
        assertEquals(15, smallMonster.getHealth());
        assertEquals(95, character.getHealth());

        battleService.calculateAttackDamage(character, smallMonster);
        assertEquals(-20, smallMonster.getHealth());
        assertEquals(150, character.getHealth());


    }

    @Test
    public void testAttackDefense() {
        Character character = new Character.Builder("Test").build();
        Monster smallMonster = new SmallMonster();

        battleService.calculateAttackDefense(character, smallMonster);
        assertEquals(50, smallMonster.getHealth());
        assertEquals(96, character.getHealth());
    }

    @Test
    public void testTakeDirectDamage() {
        Character character = new Character.Builder("Test").build();
        Monster smallMonster = new SmallMonster();

        battleService.takeDirectDamage(character, smallMonster);
        assertEquals(50, smallMonster.getHealth());
        assertEquals(95, character.getHealth());
    }

    @Test
    public void testTakeDirectDamageAfterRest() {
        Character character = new Character.Builder("Test").build();
        Monster smallMonster = new SmallMonster();

        battleService.takeDirectDamage(character, smallMonster);
        assertEquals(50, smallMonster.getHealth());
        assertEquals(95, character.getHealth());

        battleService.takeDirectDamage(character, smallMonster);
        battleService.takeDirectDamage(character, smallMonster);
        battleService.takeDirectDamage(character, smallMonster);
        assertEquals(50, smallMonster.getHealth());
        assertEquals(80, character.getHealth());

        character.rest();
        assertEquals(95, character.getHealth());

        character.rest();
        assertEquals(100, character.getHealth());
    }
}