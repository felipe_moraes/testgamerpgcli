package br.com.fmoraes.rpgcli.domain.model.exp;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExperienceTest {

    @Test
    public void createExperience() {
        Experience experience = new Experience();

        assertEquals(1, experience.getLevel());
        assertEquals(0, experience.getQuantity());
        assertEquals(10, experience.getToNextLevel());
    }

    @Test
    public void shouldIncreaseTheLevelBy1WhenReceiveExp() {
        Experience experience = new Experience();

        experience.add(10);

        assertEquals(2, experience.getLevel());
        assertEquals(10, experience.getQuantity());
        assertEquals(40, experience.getToNextLevel());
    }

    @Test
    public void shouldIncreaseTheLeve2By1WhenReceiveExp() {
        Experience experience = new Experience();

        experience.add(40);

        assertEquals(3, experience.getLevel());
        assertEquals(40, experience.getQuantity());
        assertEquals(240, experience.getToNextLevel());
    }
}