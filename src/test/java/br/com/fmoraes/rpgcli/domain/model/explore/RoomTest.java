package br.com.fmoraes.rpgcli.domain.model.explore;

import org.junit.Test;

import static org.junit.Assert.*;

public class RoomTest {

    @Test
    public void createRoomWithTreasure() {
        Room room = new Room(true);

        assertTrue(room.hasTreasure());
    }

    @Test
    public void createRoomWithoutTreasure() {
        Room room = new Room(false);

        assertFalse(room.hasTreasure());
    }

}