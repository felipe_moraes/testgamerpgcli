package br.com.fmoraes.rpgcli.domain.model.character;

import br.com.fmoraes.rpgcli.domain.model.explore.BasicRoute;
import org.junit.Test;

import static org.junit.Assert.*;

public class CharacterTest {

    @Test
    public void shouldCreateCharacterUsingBuilder() {
        Character newCharacter = new Character.Builder("Felipe").build();

        assertNotNull(newCharacter);
        assertEquals("Felipe", newCharacter.getName());
    }

    @Test
    public void characterShouldExploreRoute() {
        Character character = new Character.Builder("Felipe").build();

        character.explore(new BasicRoute("Test Route", null));

        assertEquals("Test Route", character.currentRoute());
    }

    @Test
    public void characterShouldLeaveRoute() {
        Character character = new Character.Builder("Felipe").build();

        character.explore(new BasicRoute("Test Route", null));
        character.leaveRoute();
        assertEquals("Not exploring at the moment", character.currentRoute());
    }
}