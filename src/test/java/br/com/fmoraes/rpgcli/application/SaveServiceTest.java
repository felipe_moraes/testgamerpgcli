package br.com.fmoraes.rpgcli.application;

import br.com.fmoraes.rpgcli.domain.model.character.Character;
import org.junit.After;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;

import static org.junit.Assert.*;

public class SaveServiceTest {

    private SaveService saveService = new DefaultSaveService();

    @After
    public void tearDown() {
        File file = getFile();
        if (file.exists()) {
            file.delete();
        }
    }

    @Test
    public void saveCharacter() {
        InternalCache.addCharacter(new Character.Builder("Test").build());

        saveService.save();

        File file = getFile();
        assertTrue(file.exists());
    }

    @Test
    public void loadCharacter() throws FileNotFoundException {
        InternalCache.addCharacter(new Character.Builder("Test").build());
        saveService.save();
        InternalCache.addCharacter(null);

        saveService.load();

        Character character = InternalCache.getCurrentCharacter();
        assertNotNull(character);
        assertEquals("Test", character.getName());
        assertEquals(100, character.getHealth());
        assertEquals(50, character.getAttack());
        assertEquals(25, character.getDefense());
        assertEquals(1, character.getLevel());
    }

    private File getFile() {
        return new File(DefaultSaveService.FILE_PATH);
    }
}