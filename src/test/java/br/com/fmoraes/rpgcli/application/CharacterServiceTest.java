package br.com.fmoraes.rpgcli.application;

import br.com.fmoraes.rpgcli.domain.model.character.Character;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CharacterServiceTest {

    private CharacterService characterService;

    @Before
    public void setup() {
        characterService = new DefaultCharacterService();
    }

    @Test
    public void createCharacter() {
        Character character = characterService.createCharacter("Felipe");

        assertNotNull(character);
        assertEquals("Felipe", character.getName());
    }
}