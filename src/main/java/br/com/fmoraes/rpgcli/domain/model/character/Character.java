package br.com.fmoraes.rpgcli.domain.model.character;

import br.com.fmoraes.rpgcli.domain.model.exp.Experience;
import br.com.fmoraes.rpgcli.domain.model.explore.Route;
import br.com.fmoraes.rpgcli.domain.model.shared.Status;

import java.io.Serializable;

public class Character implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private Route route;
    private Status status;
    private Experience experience;

    Character() {}

    private Character(String name) {
        this.name = name;
        this.status = new Status();
        this.experience = new Experience();
        this.experience.addObserver(this.status);
    }

    public String getName() {
        return name;
    }

    public void explore(Route route) {
        this.route = route;
    }

    public void leaveRoute() {
        this.route = null;
    }

    public String currentRoute() {
        if (this.route == null) {
            return "Not exploring at the moment";
        }
        return this.route.getName();
    }

    public void receiveExp(int quantity) {
        this.experience.add(quantity);
    }

    public int attack() {
        return this.status.getAttack();
    }

    public int defend() {
        return this.status.getDefense() + this.experience.getLevel();
    }

    public void rest() {
        this.status.recover(this.experience.getLevel() * 15);
    }

    public void takeDamage(int amount) {
        if (!this.status.takeDamage(amount)) {
            throw new CharacterDiedException("You lost!");
        }
    }

    public int getAttack() {
        return this.status.getAttack();
    }

    public int getDefense() {
        return this.status.getDefense();
    }

    public int getHealth() {
        return this.status.getHealth();
    }

    public int getLevel() {
        return this.experience.getLevel();
    }

    @Override
    public String toString() {
        return "Character{" +
                "name='" + name + '\'' +
                ", route=" + currentRoute() +
                ", experience=" + experience +
                ", status=" + status +
                '}';
    }


    public static class Builder {
        private String characterName;

        public Builder(String characterName) {
            this.characterName = characterName;
        }

        public Character build() {
            return new Character(this.characterName);
        }
    }
}
