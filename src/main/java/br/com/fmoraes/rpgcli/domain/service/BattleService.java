package br.com.fmoraes.rpgcli.domain.service;

import br.com.fmoraes.rpgcli.domain.model.character.Character;
import br.com.fmoraes.rpgcli.domain.model.monsters.Monster;

public class BattleService {

    public boolean calculateAttackDamage(Character character, Monster monster) {
        int monsterDamage = character.attack() - monster.getDefense();
        boolean isMonsterAlive = monster.takeDamage(Math.abs(monsterDamage));
        if (isMonsterAlive) {
            this.takeDirectDamage(character, monster);
        } else {
            character.receiveExp(monster.getAmountExperience());
            return true;
        }
        return false;
    }

    public void calculateAttackDefense(Character character, Monster monster) {
        int characterDamage = character.defend() - monster.attack();
        character.takeDamage(Math.abs(characterDamage));
    }

    public void takeDirectDamage(Character character, Monster monster) {
        int characterDamage = character.getDefense() - monster.attack();
        character.takeDamage(Math.abs(characterDamage));
    }
}
