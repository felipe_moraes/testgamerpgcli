package br.com.fmoraes.rpgcli.domain.model.monsters;

public interface Monster {

    String getName();

    int getHealth();
    /**
     * Calculates de Damage in the Health
     *
     * @param amount - amount of damage
     * @return boolean - If the target of the damage is still alive
     */
    boolean takeDamage(int amount);

    int getDefense();

    int attack();

    int getAmountExperience();
}
