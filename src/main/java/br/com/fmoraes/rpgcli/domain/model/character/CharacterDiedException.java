package br.com.fmoraes.rpgcli.domain.model.character;

public class CharacterDiedException extends RuntimeException {

    public CharacterDiedException(String message) {
        super(message);
    }
}
