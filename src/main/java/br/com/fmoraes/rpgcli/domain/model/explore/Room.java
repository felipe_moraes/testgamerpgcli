package br.com.fmoraes.rpgcli.domain.model.explore;

public class Room {

    private boolean treasure;

    public Room(boolean treasure) {
        this.treasure = treasure;
    }

    public boolean hasTreasure() {
        return treasure;
    }
}
