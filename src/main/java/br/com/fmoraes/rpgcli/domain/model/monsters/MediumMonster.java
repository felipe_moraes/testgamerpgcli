package br.com.fmoraes.rpgcli.domain.model.monsters;

import br.com.fmoraes.rpgcli.domain.model.exp.Experience;
import br.com.fmoraes.rpgcli.domain.model.shared.Status;

public class MediumMonster implements Monster {

    private Experience experience;
    private Status status;

    public MediumMonster() {
        this.experience = new Experience(1, 25, 0);
        this.status = new Status(75, 45, 30);
    }

    @Override
    public int getAmountExperience() {
        return this.experience.getQuantity();
    }

    @Override
    public boolean takeDamage(int amount) {
        return this.status.takeDamage(amount);
    }

    @Override
    public int attack() {
        return this.status.getAttack();
    }

    @Override
    public int getDefense() {
        return this.status.getDefense();
    }

    @Override
    public String getName() {
        return "Medium Monster";
    }

    @Override
    public int getHealth() {
        return this.status.getHealth();
    }

    @Override
    public String toString() {
        return "MediumMonster{" +
                "experience=" + experience +
                ", status=" + status +
                '}';
    }
}
