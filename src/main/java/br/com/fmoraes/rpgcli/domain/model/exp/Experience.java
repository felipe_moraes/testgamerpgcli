package br.com.fmoraes.rpgcli.domain.model.exp;

import java.io.Serializable;
import java.util.Observable;

public class Experience extends Observable implements Serializable {
    private static final long serialVersionUID = 1L;

    private int level;
    private int quantity;
    private int toNextLevel;

    public Experience() {
        this(1, 0, 10);
    }

    public Experience(int level, int quantity, int toNextLevel) {
        this.level = level;
        this.quantity = quantity;
        this.toNextLevel = toNextLevel;
    }

    public void add(int quantity) {
        while (this.quantity + quantity >= this.toNextLevel) {
            levelUp();
        }
        this.quantity += quantity;
    }

    private void levelUp() {
        this.level++;
        this.toNextLevel = this.level * (this.toNextLevel * 2);
        setChanged();
        notifyObservers();
    }

    public int getLevel() {
        return level;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getToNextLevel() {
        return toNextLevel;
    }

    @Override
    public String toString() {
        return "Experience{" +
                "level=" + level +
                ", quantity=" + quantity +
                ", toNextLevel=" + toNextLevel +
                '}';
    }
}
