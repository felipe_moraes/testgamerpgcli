package br.com.fmoraes.rpgcli.domain.model.shared;

import br.com.fmoraes.rpgcli.domain.model.exp.Experience;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public class Status implements Observer, Serializable {
    private static final long serialVersionUID = 1L;

    private int fullHealth;
    private int health;
    private int attack;
    private int defense;

    public Status() {
        this.fullHealth = 100;
        this.health = 100;
        this.attack = 50;
        this.defense = 25;
    }

    public Status(int health, int attack, int defense) {
        this.health = health;
        this.attack = attack;
        this.defense = defense;
    }

    public int getHealth() {
        return health;
    }

    /**
     * Calculates de Damage in the Health
     *
     * @param amount - amount of damage
     * @return boolean - If the target of the damage is still alive
     */
    public boolean takeDamage(int amount) {
        health -=  amount;
        return health > 0;
    }

    public int getAttack() {
        return attack;
    }

    public int getDefense() {
        return defense;
    }

    public void recover(int amount) {
        if (this.health + amount > this.fullHealth) {
            this.health = this.fullHealth;
        } else {
            this.health += amount;
        }
    }
    @Override
    public String toString() {
        return "Status{" +
                "fullHealth=" + fullHealth +
                ", health=" + health +
                ", attack=" + attack +
                ", defense=" + defense +
                '}';
    }

    @Override
    public void update(Observable expObservable, Object arg) {
        if (expObservable instanceof Experience) {
            Experience experience = (Experience) expObservable;
            int currentLevel = experience.getLevel();
            this.fullHealth += (this.fullHealth / currentLevel);
            this.attack += (this.attack / currentLevel);
            this.defense += (this.defense / currentLevel);
            this.health = this.fullHealth;
            System.out.println("Level UP!");
            System.out.println("Status increased: " + this);
        }
    }
}
