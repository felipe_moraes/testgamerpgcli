package br.com.fmoraes.rpgcli.domain.model.explore;

import java.util.List;

public interface RouteRepository {

    List<Route> findAll();
}
