package br.com.fmoraes.rpgcli.domain.model.explore;

import java.util.Collections;
import java.util.List;

public class BasicRoute implements Route {

    private String name;
    private List<Room> availableRooms;

    public BasicRoute(String name, List<Room> availableRooms) {
        this.name = name;
        this.availableRooms = availableRooms;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Room> getRooms() {
        return Collections.unmodifiableList(availableRooms);
    }

}
