package br.com.fmoraes.rpgcli.domain.model.monsters;

import java.util.List;

public interface MonsterRepository {

    List<Monster> findAll();

}
