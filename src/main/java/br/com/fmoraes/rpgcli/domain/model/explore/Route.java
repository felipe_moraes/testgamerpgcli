package br.com.fmoraes.rpgcli.domain.model.explore;

import java.util.List;

public interface Route {

    String getName();
    List<Room> getRooms();
}
