package br.com.fmoraes.rpgcli.view;

import br.com.fmoraes.rpgcli.domain.model.explore.Room;
import br.com.fmoraes.rpgcli.domain.model.explore.Route;

import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

import static br.com.fmoraes.rpgcli.view.utils.TerminalColors.*;

class RouteMenu {

    void menu(Scanner scanner, Route route) {
        RED.print("Location: " + route.getName());
        List<Room> rooms = route.getRooms();
        int qtyRooms = rooms.size();
        boolean run = true;
        while (run) {
            WHITE.print("We have " + qtyRooms + " Rooms in this route");
            WHITE.print("Choose one");
            IntStream.range(0, qtyRooms)
                    .forEach(index -> WHITE.print(String.format("%d - Room %d", index, index)));
            WHITE.print("-1 - Leave");
            int menuOption = scanner.nextInt();
            if (menuOption == -1) {
                run = false;
            } else if (menuOption >= qtyRooms) {
                RED.print("Choose the options within the range!");
            } else {
                if (rooms.get(menuOption).hasTreasure()) {
                    YELLOW.print("Great, you found a treasure!");
                    YELLOW.print("Look in other rooms? (true/false)");
                    boolean keepLooking = scanner.nextBoolean();
                    if (!keepLooking) {
                        run = false;
                    }
                } else {
                    PURPLE.print("Nothing to see in here!");
                }
            }
        }
    }
}
