package br.com.fmoraes.rpgcli.view;

import br.com.fmoraes.rpgcli.application.InternalCache;
import br.com.fmoraes.rpgcli.domain.model.monsters.Monster;
import br.com.fmoraes.rpgcli.domain.service.BattleService;

import java.util.Scanner;

import static br.com.fmoraes.rpgcli.view.utils.TerminalColors.GREEN;
import static br.com.fmoraes.rpgcli.view.utils.TerminalColors.WHITE;
import static br.com.fmoraes.rpgcli.view.utils.TerminalColors.YELLOW;

public class ArenaMenu {

    private BattleService battleService = new BattleService();

    public void menu(Scanner scanner, Monster monster) {
        boolean run = true;
        while (run) {
            GREEN.print("Monster status: " + monster);
            GREEN.print("Your status: " + InternalCache.getCurrentCharacter());

            WHITE.print("What will you do?");
            WHITE.print("1 - Attack");
            WHITE.print("2 - Defend");
            WHITE.print("3 - Rest");
            WHITE.print("-1 - Escape");
            int menuOption = scanner.nextInt();
            switch (menuOption) {
                case -1:
                    run = false;
                    break;
                case 1:
                    boolean battleFinished = battleService.calculateAttackDamage(InternalCache.getCurrentCharacter(), monster);
                    if (battleFinished) {
                        YELLOW.print("You won! You should take a rest before moving on!");
                        run = false;
                    }
                    break;
                case 2:
                    battleService.calculateAttackDefense(InternalCache.getCurrentCharacter(), monster);
                    break;
                case 3:
                    InternalCache.getCurrentCharacter().rest();
                    battleService.takeDirectDamage(InternalCache.getCurrentCharacter(), monster);
                    break;
            }
        }
    }

}
