package br.com.fmoraes.rpgcli.view;

import br.com.fmoraes.rpgcli.application.InternalCache;
import br.com.fmoraes.rpgcli.domain.model.monsters.Monster;

import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

import static br.com.fmoraes.rpgcli.view.utils.TerminalColors.RED;
import static br.com.fmoraes.rpgcli.view.utils.TerminalColors.WHITE;
import static br.com.fmoraes.rpgcli.view.utils.TerminalColors.YELLOW;

public class BattleMenu {

    private List<Monster> monsters;

    public BattleMenu(List<Monster> monsters) {
        this.monsters = monsters;
    }

    public void menu(Scanner scanner) {
        boolean run = true;
        while (run) {
            WHITE.print("Which moster do you wish to engage:");
            IntStream.range(0, monsters.size())
                    .forEach(index -> WHITE.print(String.format("%d - %s", index, monsters.get(index).getName())));
            WHITE.print("-1 - Leave");
            WHITE.print("-2 - Rest");
            int menuOption = scanner.nextInt();
            if (menuOption == -1) {
                InternalCache.getCurrentCharacter().leaveRoute();
                run = false;
            } else if (menuOption == -2) {
                YELLOW.print("Current Health: " + InternalCache.getCurrentCharacter().getHealth());
                YELLOW.print("Recovering...");
                InternalCache.getCurrentCharacter().rest();
                YELLOW.print("New Health: " + InternalCache.getCurrentCharacter().getHealth());
            } else if (menuOption >= monsters.size()) {
                RED.print("Choose the options within the range!");
            } else {
                new ArenaMenu().menu(scanner, monsters.get(menuOption));
            }
        }
    }
}
