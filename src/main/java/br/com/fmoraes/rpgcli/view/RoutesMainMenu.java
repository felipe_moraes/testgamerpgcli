package br.com.fmoraes.rpgcli.view;

import br.com.fmoraes.rpgcli.application.InternalCache;
import br.com.fmoraes.rpgcli.domain.model.explore.Route;

import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

import static br.com.fmoraes.rpgcli.view.utils.TerminalColors.RED;
import static br.com.fmoraes.rpgcli.view.utils.TerminalColors.WHITE;

public class RoutesMainMenu {

    private List<Route> routes;

    public RoutesMainMenu(List<Route> routes) {
        this.routes = routes;
    }

    public void menu(Scanner scanner) {
        boolean run = true;
        while (run) {
            WHITE.print("Choose one of the available routes:");
            IntStream.range(0, routes.size())
                    .forEach(index -> WHITE.print(String.format("%d - %s", index, routes.get(index).getName())));
            WHITE.print("-1 - Leave");
            int menuOption = scanner.nextInt();
            if (menuOption == -1) {
                InternalCache.getCurrentCharacter().leaveRoute();
                run = false;
            } else if (menuOption >= routes.size()) {
                RED.print("Choose the options within the range!");
            } else {
                InternalCache.getCurrentCharacter().explore(routes.get(menuOption));
                new RouteMenu().menu(scanner, routes.get(menuOption));
            }
        }
    }
}
