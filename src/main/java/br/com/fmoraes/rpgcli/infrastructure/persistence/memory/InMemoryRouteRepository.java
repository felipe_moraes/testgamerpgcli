package br.com.fmoraes.rpgcli.infrastructure.persistence.memory;

import br.com.fmoraes.rpgcli.domain.model.explore.BasicRoute;
import br.com.fmoraes.rpgcli.domain.model.explore.Room;
import br.com.fmoraes.rpgcli.domain.model.explore.Route;
import br.com.fmoraes.rpgcli.domain.model.explore.RouteRepository;

import java.util.ArrayList;
import java.util.List;

public class InMemoryRouteRepository implements RouteRepository {

    private static List<Route> routes = new ArrayList<>();

    static {
        routes.add(new BasicRoute("Basic Route", basicRouteRooms()));
    }

    @Override
    public List<Route> findAll() {
        return routes;
    }

    private static List<Room> basicRouteRooms() {
        List<Room> rooms = new ArrayList<>();
        rooms.add(new Room(true));
        rooms.add(new Room(false));
        rooms.add(new Room(true));
        rooms.add(new Room(false));
        rooms.add(new Room(true));
        return rooms;
    }
}
