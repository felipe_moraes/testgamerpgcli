package br.com.fmoraes.rpgcli.infrastructure.persistence.memory;

import br.com.fmoraes.rpgcli.domain.model.monsters.MediumMonster;
import br.com.fmoraes.rpgcli.domain.model.monsters.Monster;
import br.com.fmoraes.rpgcli.domain.model.monsters.MonsterRepository;
import br.com.fmoraes.rpgcli.domain.model.monsters.SmallMonster;

import java.util.ArrayList;
import java.util.List;

public class InMemoryMonsterRepository implements MonsterRepository {

    private static List<Monster> monsters = new ArrayList<>();

    static {
        monsters.add(new SmallMonster());
        monsters.add(new MediumMonster());
    }

    @Override
    public List<Monster> findAll() {
        return monsters;
    }


}
