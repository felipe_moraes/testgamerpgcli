package br.com.fmoraes.rpgcli;

import br.com.fmoraes.rpgcli.application.CharacterService;
import br.com.fmoraes.rpgcli.application.InternalCache;
import br.com.fmoraes.rpgcli.application.RoutesService;
import br.com.fmoraes.rpgcli.application.SaveService;
import br.com.fmoraes.rpgcli.domain.model.character.Character;
import br.com.fmoraes.rpgcli.domain.model.monsters.MonsterRepository;
import br.com.fmoraes.rpgcli.infrastructure.persistence.memory.InMemoryMonsterRepository;
import br.com.fmoraes.rpgcli.view.BattleMenu;
import br.com.fmoraes.rpgcli.view.RoutesMainMenu;

import java.io.FileNotFoundException;
import java.util.Scanner;

import static br.com.fmoraes.rpgcli.view.utils.TerminalColors.*;

class GameEngine {

    private CharacterService characterService;
    private RoutesService routesService;
    private MonsterRepository monsterRepository = new InMemoryMonsterRepository();
    private SaveService saveService;

    GameEngine(CharacterService characterService, RoutesService routesService, SaveService saveService) {
        this.characterService = characterService;
        this.routesService = routesService;
        this.saveService = saveService;
    }

    void start() {
        Scanner scanner = new Scanner(System.in);
        YELLOW.print("Choose a name for you character: ");
        String name = scanner.next();
        RED.print("Creating character");
        Character character = characterService.createCharacter(name);
        RED.print("Character created!");
        GREEN.print(character.toString());
        mainMenu(scanner);
    }

    void load() {
        try {
            Scanner scanner = new Scanner(System.in);
            this.saveService.load();
            GREEN.print("Welcome back: " + InternalCache.getCurrentCharacter());
            BLUE.print("You will be transported to the Initial Menu!");
            mainMenu(scanner);
        } catch (FileNotFoundException e) {
            RED.print(e.getMessage());
            RED.print("Starting from the beginning!");
            this.start();
        }
    }

    private void mainMenu(Scanner scanner) {
        boolean run = true;
        while (run) {
            WHITE.print("What do you want to do:");
            WHITE.print("1 - Explore a dungeon");
            WHITE.print("2 - Battle a monster");
            WHITE.print("3 - Save");
            WHITE.print("4 - Exit");
            int menuOption = scanner.nextInt();
            switch (menuOption) {
                case 1:
                    new RoutesMainMenu(routesService.explorationRoutes()).menu(scanner);
                    break;
                case 2:
                    new BattleMenu(monsterRepository.findAll()).menu(scanner);
                    break;
                case 3:
                    RED.print("Saving Game...");
                    this.saveService.save();
                    RED.print("Saved!");
                    break;
                case 4:
                    run = false;
                    break;
            }
        }
    }

}
