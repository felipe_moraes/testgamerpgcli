package br.com.fmoraes.rpgcli.application;

import br.com.fmoraes.rpgcli.domain.model.character.Character;

import java.io.*;

public class DefaultSaveService implements SaveService {

    static final String FILE_PATH = "./data/game.dat";
    @Override
    public void save() {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(createSaveDataFile()))) {
            oos.writeObject(InternalCache.getCurrentCharacter());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void load() throws FileNotFoundException {
        File saveDataFile = readSaveDataFile();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(saveDataFile))){
            InternalCache.addCharacter((Character) ois.readObject());
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private File createSaveDataFile() {
        File file = new File(FILE_PATH);
        if (!file.exists()) {
            try {
                file.getParentFile().mkdirs();
                file.createNewFile();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return file;
    }

    private File readSaveDataFile() throws FileNotFoundException {
        File file = new File(FILE_PATH);
        if (file.exists()) {
            return file;
        }
        throw new FileNotFoundException("No saved data found!");
    }
}
