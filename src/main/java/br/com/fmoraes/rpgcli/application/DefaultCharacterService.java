package br.com.fmoraes.rpgcli.application;

import br.com.fmoraes.rpgcli.domain.model.character.Character;

public class DefaultCharacterService implements CharacterService {

    @Override
    public Character createCharacter(String name) {
        Character currentCharacter = InternalCache.getCurrentCharacter();
        if (currentCharacter == null) {
            currentCharacter = new Character.Builder(name).build();
            InternalCache.addCharacter(currentCharacter);
        }
        return currentCharacter;
    }
}
