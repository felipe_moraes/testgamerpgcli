package br.com.fmoraes.rpgcli.application;

import br.com.fmoraes.rpgcli.domain.model.explore.Route;
import br.com.fmoraes.rpgcli.domain.model.explore.RouteRepository;

import java.util.List;

public class DefaultRoutesService implements RoutesService {

    private RouteRepository routeRepository;

    public DefaultRoutesService(RouteRepository routeRepository) {
        this.routeRepository = routeRepository;
    }

    @Override
    public List<Route> explorationRoutes() {
        return routeRepository.findAll();
    }


}
