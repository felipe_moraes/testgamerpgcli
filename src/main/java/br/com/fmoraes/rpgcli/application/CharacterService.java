package br.com.fmoraes.rpgcli.application;

import br.com.fmoraes.rpgcli.domain.model.character.Character;

public interface CharacterService {

    Character createCharacter(String name);
}
