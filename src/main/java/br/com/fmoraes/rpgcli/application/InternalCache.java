package br.com.fmoraes.rpgcli.application;

import br.com.fmoraes.rpgcli.domain.model.character.Character;

public class InternalCache {
    private static Character INSTANCE;

    private InternalCache(){

    }

    public static Character getCurrentCharacter() {
        return INSTANCE;
    }

    static void addCharacter(Character character) {
        INSTANCE = character;
    }
}
