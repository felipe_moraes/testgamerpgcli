package br.com.fmoraes.rpgcli.application;

import java.io.FileNotFoundException;

public interface SaveService {

    void save();
    void load() throws FileNotFoundException;
}
