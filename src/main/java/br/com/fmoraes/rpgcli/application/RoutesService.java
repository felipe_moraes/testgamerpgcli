package br.com.fmoraes.rpgcli.application;

import br.com.fmoraes.rpgcli.domain.model.explore.Route;

import java.util.List;

public interface RoutesService {

    List<Route> explorationRoutes();
}
