package br.com.fmoraes.rpgcli;

import br.com.fmoraes.rpgcli.application.DefaultCharacterService;
import br.com.fmoraes.rpgcli.application.DefaultRoutesService;
import br.com.fmoraes.rpgcli.application.DefaultSaveService;
import br.com.fmoraes.rpgcli.infrastructure.persistence.memory.InMemoryRouteRepository;

import java.util.Scanner;

import static br.com.fmoraes.rpgcli.view.utils.TerminalColors.*;

public class Main {

    public static void main(String[] args) {
        if (args == null || args.length < 1) {
            throw new IllegalArgumentException("To start, please provide the Player name!");
        }

        BLUE.print("Welcome player: " + args[0]);
        begin();

    }

    private static void begin() {
        Scanner scanner = new Scanner(System.in);
        BLUE.print("Before you start, do you wish to resume an old game?");
        YELLOW.print("Type true or false:");
        String resumeGame = scanner.next();
        Boolean resume = Boolean.valueOf(resumeGame);
        if (resume) {
            loadGame();
        } else {
            newGame();
        }
    }

    private static void newGame() {
        RED.print("Starting new game!");
        new GameEngine(new DefaultCharacterService(),
                new DefaultRoutesService(new InMemoryRouteRepository()),
                new DefaultSaveService())
                .start();
    }

    private static void loadGame() {
        RED.print("Loading game!");
        new GameEngine(new DefaultCharacterService(),
                new DefaultRoutesService(new InMemoryRouteRepository()),
                new DefaultSaveService())
                .load();

    }
}
