# RPGCLI

This project is a code challenge from a company. The purpose of the project is to create a command line based role playing game.

To run the project, first you need to build it. To do so, run this command `mvn clean package`. We assume you have java 
and maven installed.

After the build, a `jar` file will be created inside the target package named `rpgcli.jar`. To run it you need to use this command:
`java -jar rpgcli.jar <Your Name>`. If you don't pass the name you will receive an `IllegalArgumentException` and the game will not start.

## Test Coverage

To check the test coverage, you can run the command `mvn cobertura:cobertura`. It will run the tests and generate a html page with
the statistics about the coverage. For reasons of really hard to test I excluded the view classes from the coverage.

Also, I tried to cover the business rules and some use cases that adds value to the code, I didn't try to cover every line of code possible.

## Gameplay

The game is really simple, when you start it will ask if you want to load and old game or start a new one.

The whole game is based on menus that you take actions on. So it's possible to explore routes, fight monsters, save and quit the game.

When you battler monster when you win, you get experience based on the amount of experience that that monster holds. If it
surpasses the amount needed for you to level up, your Character levels up. (The experience is an Observable class that is Observed by the Character Status)

## Design

I tried to follow some clean architecture/DDD (both of these areas I'm still studying) approach during development, so some misconceptions
may be found in the code. But basically I tried to avoid that code from the lower levels knows about the upper levels (packages).

This is and idea of the structure: View -> Application -> Domain. Infrastructure package implements the interfaces for Repository so far and it's a package that 
can have access to all other packages.

### Extension

I created some interfaces where I found that was necessary for extensibility reasons. For instance, if you want to create another route you would have to implement
the `Route` interface, and by implementing its methods the menu would already be aware of this new Route. OBS: You'd need to add this new Route in the 
`InMemoryRouteRepository` for this to work.

Same thing with the `Monster` interface. You can create lots of new monster with different Status, and add then on `InMemoryMonsterRepository` to test.

There are also other interfaces that can be implemented to change the default behaviors if needed.
